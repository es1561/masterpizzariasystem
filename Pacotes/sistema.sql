﻿delete from sistema;
delete from funcionario;
delete from cidade;
delete from estado;
delete from mesa;

--sistema
INSERT INTO sistema(nome_empresa, nome_fantasia, razao_social, fone, cor_fundo, cor_frente, cor_botao) VALUES('teste', 'meh', 'huehue', '1', '  FFC08E', '  D25D00', 'FF811D');

--estados
INSERT INTO estado("est_cod", "est_sgl", "est_nome") VALUES (23, 'RR', 'Roraima');
INSERT INTO estado("est_cod", "est_sgl", "est_nome") VALUES (24, 'SC', 'Santa Catarina');
INSERT INTO estado("est_cod", "est_sgl", "est_nome") VALUES (25, 'SP', 'São Paulo');

--cidades
INSERT INTO cidade("cid_cod", "est_cod", "cid_nome") VALUES (4341, 23, 'Alto Alegre');
INSERT INTO cidade("cid_cod", "est_cod", "cid_nome") VALUES (4342, 23, 'Amajari');
INSERT INTO cidade("cid_cod", "est_cod", "cid_nome") VALUES (4343, 23, 'Boa Vista');
INSERT INTO cidade("cid_cod", "est_cod", "cid_nome") VALUES (4356, 24, 'Abdon Batista');
INSERT INTO cidade("cid_cod", "est_cod", "cid_nome") VALUES (4357, 24, 'Abelardo Luz');
INSERT INTO cidade("cid_cod", "est_cod", "cid_nome") VALUES (4358, 24, 'Agrolândia');
INSERT INTO cidade("cid_cod", "est_cod", "cid_nome") VALUES (4649, 25, 'Adamantina');
INSERT INTO cidade("cid_cod", "est_cod", "cid_nome") VALUES (4650, 25, 'Adolfo');
INSERT INTO cidade("cid_cod", "est_cod", "cid_nome") VALUES (4651, 25, 'Aguaí');

--funcionario
INSERT INTO funcionario(func_cod, func_nome, func_cpf, func_login, func_senha, func_nivel, func_ativo, func_cidade) VALUES(1, 'Luis', '419.503.448-51', 'admin', 'admin', 2, 'T', 4649);

--mesas
INSERT INTO mesa(mesa_cod, mesa_status) VALUES(1, 'L');
INSERT INTO mesa(mesa_cod, mesa_status) VALUES(2, 'L');
INSERT INTO mesa(mesa_cod, mesa_status) VALUES(3, 'L');
INSERT INTO mesa(mesa_cod, mesa_status) VALUES(4, 'L');
INSERT INTO mesa(mesa_cod, mesa_status) VALUES(6, 'L');