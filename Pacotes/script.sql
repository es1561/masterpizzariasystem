
CREATE SEQUENCE public.estado_est_cod_seq_1;

CREATE TABLE public.estado (
                est_cod INTEGER NOT NULL DEFAULT nextval('public.estado_est_cod_seq_1'),
                est_sgl CHAR(2) NOT NULL,
                est_nome VARCHAR(30) NOT NULL,
                CONSTRAINT est_cod PRIMARY KEY (est_cod)
);


ALTER SEQUENCE public.estado_est_cod_seq_1 OWNED BY public.estado.est_cod;

CREATE SEQUENCE public.cidade_cid_cod_seq_1;

CREATE TABLE public.cidade (
                cid_cod INTEGER NOT NULL DEFAULT nextval('public.cidade_cid_cod_seq_1'),
                cid_nome VARCHAR(30) NOT NULL,
                est_cod INTEGER NOT NULL,
                CONSTRAINT cid_cod PRIMARY KEY (cid_cod)
);


ALTER SEQUENCE public.cidade_cid_cod_seq_1 OWNED BY public.cidade.cid_cod;

CREATE SEQUENCE public.funcionario_func_cod_seq_2;

CREATE TABLE public.funcionario (
                func_cod INTEGER NOT NULL DEFAULT nextval('public.funcionario_func_cod_seq_2'),
                func_nome VARCHAR(30) NOT NULL,
                func_cpf VARCHAR(15) NOT NULL,
                func_fone VARCHAR(15),
                func_email VARCHAR(50),
                func_login VARCHAR(20) NOT NULL,
                func_senha VARCHAR(20) NOT NULL,
                func_nivel INTEGER NOT NULL,
                func_ativo CHAR(1) NOT NULL,
                func_cidade INTEGER NOT NULL,
                func_logradouro VARCHAR(100),
                CONSTRAINT func_cod PRIMARY KEY (func_cod)
);


ALTER SEQUENCE public.funcionario_func_cod_seq_2 OWNED BY public.funcionario.func_cod;

CREATE TABLE public.sistema (
                nome_empresa VARCHAR(50) NOT NULL,
                nome_fantasia VARCHAR(50),
                razao_social VARCHAR(50) NOT NULL,
                fone VARCHAR(15) NOT NULL,
                logo_grande BYTEA,
                logo_pequeno BYTEA,
                cor_fundo VARCHAR(10) NOT NULL,
                cor_frente VARCHAR(10) NOT NULL,
                cor_botao VARCHAR(10) NOT NULL
);


CREATE SEQUENCE public.fornecedor_for_cod_seq_1;

CREATE TABLE public.fornecedor (
                for_cod INTEGER NOT NULL DEFAULT nextval('public.fornecedor_for_cod_seq_1'),
                for_nome VARCHAR(20) NOT NULL,
                for_vendedor VARCHAR(20) NOT NULL,
                for_fone VARCHAR(20) NOT NULL,
                for_email VARCHAR(30) NOT NULL,
                CONSTRAINT for_cod PRIMARY KEY (for_cod)
);


ALTER SEQUENCE public.fornecedor_for_cod_seq_1 OWNED BY public.fornecedor.for_cod;

CREATE SEQUENCE public.compra_comp_cod_seq_1;

CREATE TABLE public.compra (
                comp_cod REAL NOT NULL DEFAULT nextval('public.compra_comp_cod_seq_1'),
                comp_parc INTEGER NOT NULL,
                comp_data DATE NOT NULL,
                comp_total NUMERIC(5,2) NOT NULL,
                func_cod INTEGER NOT NULL,
                for_cod INTEGER NOT NULL,
                CONSTRAINT comp_cod PRIMARY KEY (comp_cod)
);


ALTER SEQUENCE public.compra_comp_cod_seq_1 OWNED BY public.compra.comp_cod;

CREATE SEQUENCE public.produto_prod_cod_seq_1;

CREATE TABLE public.produto (
                prod_cod INTEGER NOT NULL DEFAULT nextval('public.produto_prod_cod_seq_1'),
                prod_nome VARCHAR(20) NOT NULL,
                prod_desc VARCHAR(30) NOT NULL,
                prod_tipo CHAR(1) NOT NULL,
                prod_valor NUMERIC(5,2) NOT NULL,
                CONSTRAINT prod_cod PRIMARY KEY (prod_cod)
);


ALTER SEQUENCE public.produto_prod_cod_seq_1 OWNED BY public.produto.prod_cod;

CREATE TABLE public.itemcompra (
                prod_cod INTEGER NOT NULL,
                comp_cod REAL NOT NULL,
                itemc_quant INTEGER NOT NULL,
                itemc_valor NUMERIC(5,2) NOT NULL,
                CONSTRAINT itemc_cod PRIMARY KEY (prod_cod, comp_cod)
);


CREATE SEQUENCE public.estoque_esto_lote_seq;

CREATE TABLE public.estoque (
                esto_lote INTEGER NOT NULL DEFAULT nextval('public.estoque_esto_lote_seq'),
                prod_cod INTEGER NOT NULL,
                esto_data_val DATE NOT NULL,
                esto_quant INTEGER NOT NULL,
                CONSTRAINT esto_cod PRIMARY KEY (esto_lote, prod_cod)
);


ALTER SEQUENCE public.estoque_esto_lote_seq OWNED BY public.estoque.esto_lote;

CREATE TABLE public.ingrediente (
                prod_cod INTEGER NOT NULL,
                prod_ingre_cod INTEGER NOT NULL,
                ingre_quant INTEGER NOT NULL,
                CONSTRAINT ingre_cod PRIMARY KEY (prod_cod, prod_ingre_cod)
);


CREATE TABLE public.mesa (
                mesa_cod INTEGER NOT NULL,
                mesa_status CHAR(1) NOT NULL,
                CONSTRAINT mesa_cod PRIMARY KEY (mesa_cod)
);


CREATE SEQUENCE public.comanda_com_cod_seq_1;

CREATE TABLE public.comanda (
                com_cod INTEGER NOT NULL DEFAULT nextval('public.comanda_com_cod_seq_1'),
                com_nome VARCHAR(50) NOT NULL,
                com_status CHAR(1) NOT NULL,
                com_data DATE NOT NULL,
                com_total NUMERIC(5,2) NOT NULL,
                mesa_cod INTEGER,
                func_cod INTEGER NOT NULL,
                CONSTRAINT com_cod PRIMARY KEY (com_cod)
);


ALTER SEQUENCE public.comanda_com_cod_seq_1 OWNED BY public.comanda.com_cod;

CREATE TABLE public.itemvenda (
                com_cod INTEGER NOT NULL,
                prod_cod INTEGER NOT NULL,
                itemv_quant INTEGER NOT NULL,
                itemv_valor NUMERIC(5,2) NOT NULL,
                CONSTRAINT itemv_cod PRIMARY KEY (com_cod, prod_cod)
);


CREATE TABLE public.caixa (
                caixa_data DATE NOT NULL,
                caixa_valor_a NUMERIC(5,2) NOT NULL,
                caixa_valor_f NUMERIC(5,2) NOT NULL,
                caixa_status CHAR(1) NOT NULL,
                CONSTRAINT caixa_data PRIMARY KEY (caixa_data)
);


CREATE SEQUENCE public.recebimento_rec_cod_seq;

CREATE TABLE public.recebimento (
                rec_cod INTEGER NOT NULL DEFAULT nextval('public.recebimento_rec_cod_seq'),
                com_cod INTEGER NOT NULL,
                rec_data DATE NOT NULL,
                rec_valor NUMERIC(5,2) NOT NULL,
                caixa_data DATE NOT NULL,
                CONSTRAINT rec_cod PRIMARY KEY (rec_cod, com_cod)
);


ALTER SEQUENCE public.recebimento_rec_cod_seq OWNED BY public.recebimento.rec_cod;

CREATE SEQUENCE public.pagamento_pag_cod_seq;

CREATE TABLE public.pagamento (
                pag_cod INTEGER NOT NULL DEFAULT nextval('public.pagamento_pag_cod_seq'),
                comp_cod REAL NOT NULL,
                pag_valor NUMERIC(5,2) NOT NULL,
                pag_datav DATE NOT NULL,
                pag_datap DATE NOT NULL,
                caixa_data DATE NOT NULL,
                CONSTRAINT pag_cod PRIMARY KEY (pag_cod, comp_cod)
);


ALTER SEQUENCE public.pagamento_pag_cod_seq OWNED BY public.pagamento.pag_cod;

CREATE SEQUENCE public.movimento_mov_cod_seq;

CREATE TABLE public.movimento (
                mov_cod INTEGER NOT NULL DEFAULT nextval('public.movimento_mov_cod_seq'),
                caixa_data DATE NOT NULL,
                mov_data DATE NOT NULL,
                mov_valor NUMERIC(5,2) NOT NULL,
                CONSTRAINT mov_cod PRIMARY KEY (mov_cod, caixa_data)
);


ALTER SEQUENCE public.movimento_mov_cod_seq OWNED BY public.movimento.mov_cod;

ALTER TABLE public.cidade ADD CONSTRAINT estado_cidade_fk
FOREIGN KEY (est_cod)
REFERENCES public.estado (est_cod)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.funcionario ADD CONSTRAINT cidade_funcionario_fk
FOREIGN KEY (func_cidade)
REFERENCES public.cidade (cid_cod)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.comanda ADD CONSTRAINT funcionario_comanda_fk
FOREIGN KEY (func_cod)
REFERENCES public.funcionario (func_cod)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.compra ADD CONSTRAINT funcionario_compra_fk
FOREIGN KEY (func_cod)
REFERENCES public.funcionario (func_cod)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.compra ADD CONSTRAINT fornecedor_compra_fk
FOREIGN KEY (for_cod)
REFERENCES public.fornecedor (for_cod)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.pagamento ADD CONSTRAINT compra_pagamento_fk
FOREIGN KEY (comp_cod)
REFERENCES public.compra (comp_cod)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.itemcompra ADD CONSTRAINT compra_itemcompra_fk
FOREIGN KEY (comp_cod)
REFERENCES public.compra (comp_cod)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.itemvenda ADD CONSTRAINT produto_itemvenda_fk
FOREIGN KEY (prod_cod)
REFERENCES public.produto (prod_cod)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.ingrediente ADD CONSTRAINT produto_ingrediente_fk
FOREIGN KEY (prod_cod)
REFERENCES public.produto (prod_cod)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.ingrediente ADD CONSTRAINT produto_ingrediente_fk1
FOREIGN KEY (prod_ingre_cod)
REFERENCES public.produto (prod_cod)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.estoque ADD CONSTRAINT produto_estoque_fk
FOREIGN KEY (prod_cod)
REFERENCES public.produto (prod_cod)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.itemcompra ADD CONSTRAINT produto_itemcompra_fk
FOREIGN KEY (prod_cod)
REFERENCES public.produto (prod_cod)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.comanda ADD CONSTRAINT mesa_comanda_fk
FOREIGN KEY (mesa_cod)
REFERENCES public.mesa (mesa_cod)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.recebimento ADD CONSTRAINT comanda_recebimento_fk
FOREIGN KEY (com_cod)
REFERENCES public.comanda (com_cod)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.itemvenda ADD CONSTRAINT comanda_itemvenda_fk
FOREIGN KEY (com_cod)
REFERENCES public.comanda (com_cod)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.movimento ADD CONSTRAINT caixa_movimento_fk
FOREIGN KEY (caixa_data)
REFERENCES public.caixa (caixa_data)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.pagamento ADD CONSTRAINT caixa_pagamento_fk
FOREIGN KEY (caixa_data)
REFERENCES public.caixa (caixa_data)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.recebimento ADD CONSTRAINT caixa_recebimento_fk
FOREIGN KEY (caixa_data)
REFERENCES public.caixa (caixa_data)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;
