package DAL;

import Class.Cidade;
import Class.Estado;
import Class.Funcionario;
import java.sql.SQLException;
import javafx.collections.ObservableList;

public class DALFuncionario
{
    private DALFuncionario()
    {
        
    }
    
    public static void insert(String nome, String cpf, String fone, String email, String login, String senha, int nivel, boolean ativo, Cidade cidade, String logradouro) throws SQLException
    {
        Funcionario func = new Funcionario(nivel, nome, cpf, fone, email, login, senha, nivel, ativo, cidade, logradouro);
        
        func.insert();
    }
    
    public static void update(int codigo, String nome, String cpf, String fone, String email, String login, String senha, int nivel, boolean ativo, Cidade cidade, String logradouro) throws SQLException
    {
        Funcionario func = new Funcionario(codigo, nome, cpf, fone, email, login, senha, nivel, ativo, cidade, logradouro);
        
        func.update();
    }
    
    public static void delete(int cod)
    {
        Funcionario func = new Funcionario(cod);
        
        func.delete();
    }
    
    public static Funcionario login(String login, String pass)
    {
        Funcionario func = new Funcionario();
        
        func.setLogin(login);
        func.setSenha(pass);
        
        return func.login() ? func : null;
    }
    
    public static boolean uniqueLogin(int codigo, String login, boolean f_not_me)
    {
        Funcionario func = new  Funcionario();
        
        func.setCodigo(codigo);
        func.setLogin(login);
        
        return func.uniqueLogin(f_not_me);
    }
    
    public static boolean uniqueCPF(int codigo, String CPF, boolean f_not_me)
    {
        Funcionario func = new  Funcionario();
        
        func.setCodigo(codigo);
        func.setCpf(CPF);
        
        return func.uniqueCPF(f_not_me);
    }
    
    public static ObservableList<Funcionario> searchAll()
    {
        return new Funcionario().searchAll();
    }
    
    public static Funcionario searchByCodigo(int codigo)
    {
        Funcionario func = new  Funcionario();
        
        func.setCodigo(codigo);
        
        return func.searchByCodigo();
    }
    
    public static ObservableList<Funcionario> searchByName(String name)
    {
        Funcionario func = new  Funcionario();
        
        func.setNome(name);
        
        return func.searchByName();
    }
    
    public static ObservableList<Funcionario> searchByCPF(String CPF)
    {
        Funcionario func = new  Funcionario();
        
        func.setCpf(CPF);
        
        return func.searchByCPF();
    }
    
    public static ObservableList<Funcionario> searchByActive(boolean flag)
    {
        Funcionario func = new  Funcionario();
        
        func.setAtivo(flag);
        
        return func.searchByActive();
    }
}
