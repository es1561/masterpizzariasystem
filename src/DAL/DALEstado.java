package DAL;

import Class.Estado;
import javafx.collections.ObservableList;

public class DALEstado
{
    private DALEstado()
    {
        
    }
    
    public static ObservableList<Estado> searchAll()
    {
        return new Estado().searchAll();
    }
    
    public static Estado searchBySigla(String sigla)
    {
        Estado est = new Estado();
        
        est.setSigla(sigla);
        
        return est.searchBySigla();
    }
    
    public static Estado searchByCodigo(int codigo)
    {
        Estado est = new Estado();
        
        est.setCodigo(codigo);
        
        return est.searchByCodigo();
    }
}
