package DAL;

import Class.Mesa;
import javafx.collections.ObservableList;

public class DALMesa
{
    private DALMesa()
    {
        
    }
    
    public static boolean update(int codigo, String status)
    {
        return new Mesa(codigo, status).update();
    }
    
    public static ObservableList<Mesa> searchByStatus(String status)
    {
        return new Mesa(status).searchByStatus();
    }
    
    public static Mesa searchByCodigo(int codigo)
    {
        return new Mesa(codigo);
    }
    
    public static ObservableList<Mesa> searchAll()
    {
        return new Mesa().searchAll();
    }
}
