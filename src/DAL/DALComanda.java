package DAL;

import Class.Comanda;
import Class.Funcionario;
import Class.Mesa;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import javafx.collections.ObservableList;

public class DALComanda
{
    
    public static void insert(String nome, int mesa_cod) throws SQLException
    {
        Mesa mesa = new Mesa(mesa_cod).searchByCodigo();
        Funcionario func = new Funcionario(Main.Main._user.getCodigo()).searchByCodigo();
        
        new Comanda(nome, "A", new Date(LocalDate.now().toEpochDay()), mesa, func).insert();
    }
    
    public static void close(int codigo) throws SQLException
    {
        new Comanda(codigo).close();
    }
    
    public static ObservableList<Comanda> searchByMesa(int mesa_cod)
    {
        Comanda com = new Comanda();
        
        com.setMesa(new Mesa(mesa_cod));
        
        return com.searchByMesa();
    }
    
    public static ObservableList<Comanda> searchAll()
    {
        return new Comanda().searchAll();
    }
}
