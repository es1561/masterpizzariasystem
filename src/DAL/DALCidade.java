package DAL;

import Class.Cidade;
import Class.Estado;
import javafx.collections.ObservableList;

public class DALCidade
{
    private DALCidade()
    {
        
    }
    
    public static ObservableList<Cidade> searchByEstado(int estado)
    {
        Cidade cid = new Cidade();
        
        cid.setEstado(new Estado(estado));
        
        return cid.searchByEstado();
    }
    
    public static Cidade searchByCodigo(int codigo)
    {
        Cidade cid = new Cidade();
        
        cid.setCodigo(codigo);
        
        return cid.searchByCodigo();
    }
}
