package DAL;

import Class.Cidade;
import Class.Funcionario;
import Class.Produto;
import java.sql.SQLException;
import javafx.collections.ObservableList;

public class DALProduto
{
    private DALProduto()
    {
        
    }
    
    public static void insert(String nome, double valor, char tipo,String descricao) throws SQLException
    {
        Produto prod = new Produto(nome, descricao, valor, tipo);
        
        prod.insert();
    }
    
    public static void update(int codigo, String nome, double valor, char tipo,String descricao) throws SQLException
    {
        Produto prod = new Produto(codigo, nome, descricao, valor, tipo);
        
        prod.update();
    }
    
    public static void delete(int cod) throws SQLException
    {
        Produto prod = new Produto(cod);
        
        prod.delete();
    }
    
    public static ObservableList<Produto> searchByTipo(char tipo)
    {
        Produto prod = new  Produto();
        
        prod.setTipo(tipo);
        
        return prod.searchByTipo();
    }
    
    public static ObservableList<Produto> searchByNome(String nome)
    {
        Produto prod = new  Produto();
        
        prod.setNome(nome);
        
        return prod.searchByNome();
    }
    
    public static ObservableList<Produto> searchAll()
    {
        return new Produto().searchAll();
    }
}
