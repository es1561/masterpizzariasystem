package Main;

import Class.Funcionario;
import Class.Sistema;
import Scene.FXMLLoginController;
import DataBase.Banco;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class Main extends Application
{
    public static Funcionario _user;

    @Override
    public void start(Stage stage) throws Exception
    {        
        if(Banco.conectar())
        {
            if(Sistema.reload())//load system
            {
                Parent root;
                Scene scene;
          
                root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
                scene = new Scene(root);

                stage.initStyle(StageStyle.UNDECORATED);
                stage.setScene(scene);
                stage.show();
            }
            else
            {
                //sistema...
            }
        }
        else
            System.out.println("Erro no banco");
    }

    public static void main(String[] args)
    {
        launch(args);
    }
    
}
