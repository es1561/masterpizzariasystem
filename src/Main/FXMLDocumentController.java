package Main;

import Class.Funcionario;
import Class.Sistema;
import static Main.Main._user;
import Scene.FXMLLoginController;
import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class FXMLDocumentController implements Initializable
{
    @FXML
    public Pane scene;
    @FXML
    private JFXButton btn_mesas;
    @FXML
    private JFXButton btn_func;
    @FXML
    private JFXButton btn_estoque;
    @FXML
    private JFXButton btn_for;
    @FXML
    private JFXButton btn_compra;
    @FXML
    private JFXButton btn_venda;
    @FXML
    private JFXButton btn_caixa;
    @FXML
    private JFXButton btn_contas;
    @FXML
    private JFXButton btn_sair;
    @FXML
    private JFXButton btn_cardapio;
    @FXML
    private JFXButton btn_lancar;
    @FXML
    private VBox vb_front_1;
    @FXML
    private HBox vb_front_2;
    @FXML
    private ImageView img_logo;
   
    private void load()
    {
        //menus
        img_logo.setImage(Sistema.getLogo_grande());
        vb_front_1.setStyle("-fx-background-color:" + Sistema.getCor_botao());
        vb_front_2.setStyle("-fx-background-color:" + Sistema.getCor_botao());
        scene.setStyle("-fx-background-color:" + Sistema.getCor_fundo());

        //botoes
        btn_caixa.setStyle("-fx-background-color:" + Sistema.getCor_botao());
        btn_cardapio.setStyle("-fx-background-color:" + Sistema.getCor_botao());
        btn_compra.setStyle("-fx-background-color:" + Sistema.getCor_botao());
        btn_contas.setStyle("-fx-background-color:" + Sistema.getCor_botao());
        btn_estoque.setStyle("-fx-background-color:" + Sistema.getCor_botao());
        btn_for.setStyle("-fx-background-color:" + Sistema.getCor_botao());
        btn_func.setStyle("-fx-background-color:" + Sistema.getCor_botao());
        btn_lancar.setStyle("-fx-background-color:" + Sistema.getCor_botao());
        btn_mesas.setStyle("-fx-background-color:" + Sistema.getCor_botao());
        btn_venda.setStyle("-fx-background-color:" + Sistema.getCor_botao());
        btn_sair.setStyle("-fx-background-color:" + Sistema.getCor_botao());
        
      
    }
    
    private void loginWindow()
    {
        Pane newLoadedPane;
        try
        {
            Stage stage = new Stage();
            Parent root;
            Scene scene;
              
            root = FXMLLoader.load(getClass().getResource("/Scene/FXMLLogin.fxml"));
            scene = new Scene(root);

            stage.initStyle(StageStyle.UNDECORATED);
            stage.setScene(scene);
            stage.showAndWait();
        }
        catch(IOException ex)
        {
            System.out.println(ex.getMessage());
        }
    }
    
    private void unlock(int level)
    {
        switch(level)
        {
            case 0://funcionario
                //btn_caixa.setDisable(true);
                btn_cardapio.setDisable(false);
                //btn_compra.setDisable(true);
                //btn_contas.setDisable(true);
                //btn_estoque.setDisable(true);
                //btn_for.setDisable(true);
                //btn_func.setDisable(true);
                //btn_lancar.setDisable(true);
                btn_mesas.setDisable(false);
                btn_venda.setDisable(false);
            break;
            
            case 1://gerente
                //btn_caixa.setDisable(true);
                btn_cardapio.setDisable(false);
                btn_compra.setDisable(false);
                btn_contas.setDisable(false);
                btn_estoque.setDisable(false);
                btn_for.setDisable(false);
                btn_func.setDisable(false);
                btn_lancar.setDisable(false);
                btn_mesas.setDisable(false);
                btn_venda.setDisable(false);
            break;
            
            case 2://adm
                btn_caixa.setDisable(false);
                btn_cardapio.setDisable(false);
                btn_compra.setDisable(false);
                btn_contas.setDisable(false);
                btn_estoque.setDisable(false);
                btn_for.setDisable(false);
                btn_func.setDisable(false);
                btn_lancar.setDisable(false);
                btn_mesas.setDisable(false);
                btn_venda.setDisable(false);
            break;
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        // TODO
        btn_caixa.setDisable(true);
        btn_cardapio.setDisable(true);
        btn_compra.setDisable(true);
        btn_contas.setDisable(true);
        btn_estoque.setDisable(true);
        btn_for.setDisable(true);
        btn_func.setDisable(true);
        btn_lancar.setDisable(true);
        btn_mesas.setDisable(true);
        btn_venda.setDisable(true);
        
        load();
        loginWindow();
        
        unlock(Main._user.getNivel());
    }    

    @FXML
    private void MouseOut(MouseEvent event)
    {
        
    }

    @FXML
    private void MouseIn(MouseEvent event)
    {
        
    }

    @FXML
    private void ClickMesas(ActionEvent event) throws IOException
    {
        Pane newLoadedPane = FXMLLoader.load(getClass().getResource("/Scene/FXMLMesas.fxml"));
        scene.getChildren().clear();
        scene.getChildren().add(newLoadedPane); 
    }

    @FXML
    private void ClickFunc(ActionEvent event) throws IOException
    {
        Pane newLoadedPane = FXMLLoader.load(getClass().getResource("/Scene/FXMLFuncionario.fxml"));
        scene.getChildren().clear();
        scene.getChildren().add(newLoadedPane); 
    }

    @FXML
    private void ClickEstoque(ActionEvent event) throws IOException
    {
        Pane newLoadedPane = FXMLLoader.load(getClass().getResource("/Scene/FXMLProduto.fxml"));
        scene.getChildren().clear();
        scene.getChildren().add(newLoadedPane); 
    }

    @FXML
    private void ClickFor(ActionEvent event)
    {
        
    }

    @FXML
    private void ClickCompra(ActionEvent event)
    {
        
    }

    @FXML
    private void ClickVenda(ActionEvent event)
    {
        
    }

    @FXML
    private void ClickCaixa(ActionEvent event)
    {
        
    }

    @FXML
    private void ClickContas(ActionEvent event)
    {
        
    }

    @FXML
    private void ClickSair(ActionEvent event)
    {
        System.exit(0);
    }

    @FXML
    private void ClickCardapio(ActionEvent event)
    {
        
    }

    @FXML
    private void ClickLancar(ActionEvent event)
    {
        
    }
    
}
