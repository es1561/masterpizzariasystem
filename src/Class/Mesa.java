package Class;

import DataBase.Banco;
import java.sql.ResultSet;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Mesa
{
    private int codigo;
    private String status;

    public Mesa()
    {
        
    }

    public Mesa(int codigo)
    {
        this.codigo = codigo;
    }

    public Mesa(String status)
    {
        this.status = status;
    }

    public Mesa(int codigo, String status)
    {
        this.codigo = codigo;
        this.status = status;
    }

    public int getCodigo()
    {
        return codigo;
    }

    public void setCodigo(int codigo)
    {
        this.codigo = codigo;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }
    
    public boolean update()
    {
        String sql = "UPDATE mesa SET mesa_status = '" + status + "' WHERE mesa_cod = " + codigo;
        
        return Banco.getCon().manipular(sql);
    }
    
    public Mesa searchByCodigo()
    {
        String sql = "SELECT * FROM mesa WHERE mesa_cod = " + codigo;
        ResultSet rs = Banco.getCon().consultar(sql);
        Mesa list = null;
        
        try
        {
            while(rs.next())
            {
                codigo = rs.getInt("mesa_cod");
                status = rs.getString("mesa_status");
                
                list = new Mesa(codigo, status);
            }
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
        
        return list;
    }
    
    public ObservableList<Mesa> searchByStatus()
    {
        String sql = "SELECT * FROM mesa WHERE mesa_status = '" + status + "'";
        ResultSet rs = Banco.getCon().consultar(sql);
        ObservableList<Mesa> list = FXCollections.observableArrayList();
        
        try
        {
            while(rs.next())
            {
                codigo = rs.getInt("mesa_cod");
                status = rs.getString("mesa_status");
                
                list.add(new Mesa(codigo, status));
            }
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
        
        return list;
    }
    
    public ObservableList<Mesa> searchAll()
    {
        String sql = "SELECT * FROM mesa";
        ResultSet rs = Banco.getCon().consultar(sql);
        ObservableList<Mesa> list = FXCollections.observableArrayList();
        
        try
        {
            while(rs.next())
            {
                codigo = rs.getInt("mesa_cod");
                status = rs.getString("mesa_status");
                
                list.add(new Mesa(codigo, status));
            }
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
        
        return list;
    }
}
