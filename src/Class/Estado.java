package Class;

import DataBase.Banco;
import java.sql.ResultSet;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Estado
{
    private int codigo;
    private String sigla;
    private String nome;

    public Estado()
    {
        
    }

    public Estado(int codigo)
    {
        this.codigo = codigo;
    }

    public Estado(int codigo, String sigla, String nome)
    {
        this.codigo = codigo;
        this.sigla = sigla;
        this.nome = nome;
    }

    public int getCodigo()
    {
        return codigo;
    }

    public void setCodigo(int codigo)
    {
        this.codigo = codigo;
    }

    public String getSigla()
    {
        return sigla;
    }

    public void setSigla(String sigla)
    {
        this.sigla = sigla;
    }

    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

    @Override
    public String toString()
    {
        return sigla;
    }
    
    public ObservableList<Estado> searchAll()
    {
        String sql = "SELECT * FROM estado";
        ResultSet rs = Banco.getCon().consultar(sql);
        ObservableList<Estado> list = FXCollections.observableArrayList();
        
        try
        {
            while(rs.next())
            {
                codigo = rs.getInt("est_cod");
                sigla = rs.getString("est_sgl");
                nome = rs.getString("est_nome");
                
                
                list.add(new Estado(codigo, sigla, nome));
            }
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
        
        return list;
    }
    
    public Estado searchBySigla()
    {
        String sql = "SELECT * FROM estado WHERE est_sgl = '" + sigla + "'";
        ResultSet rs = Banco.getCon().consultar(sql);
        Estado list = null;
        
        try
        {
            while(rs.next())
            {
                codigo = rs.getInt("est_cod");
                sigla = rs.getString("est_sgl");
                nome = rs.getString("est_nome");
                
                
                list = new Estado(codigo, sigla, nome);
            }
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
        
        return list;
    }
    
    public Estado searchByCodigo()
    {
        String sql = "SELECT * FROM estado WHERE est_cod = " + codigo;
        ResultSet rs = Banco.getCon().consultar(sql);
        Estado list = null;
        
        try
        {
            while(rs.next())
            {
                codigo = rs.getInt("est_cod");
                sigla = rs.getString("est_sgl");
                nome = rs.getString("est_nome");
                
                
                list = new Estado(codigo, sigla, nome);
            }
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
        
        return list;
    }
}
