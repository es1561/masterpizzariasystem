package Class;

import DAL.DALEstado;
import DataBase.Banco;
import java.sql.ResultSet;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Cidade
{
    private int codigo;
    private Estado estado;
    private String nome;

    public Cidade()
    {
        
    }

    public Cidade(int codigo)
    {
        this.codigo = codigo;
    }

    public Cidade(int codigo, Estado estado, String nome)
    {
        this.codigo = codigo;
        this.estado = estado;
        this.nome = nome;
    }

    public int getCodigo()
    {
        return codigo;
    }

    public void setCodigo(int codigo)
    {
        this.codigo = codigo;
    }

    public Estado getEstado()
    {
        return estado;
    }

    public void setEstado(Estado estado)
    {
        this.estado = estado;
    }

    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

    @Override
    public String toString()
    {
        return nome;
    }
  
    public ObservableList<Cidade> searchByEstado()
    {
        String sql = "SELECT * FROM cidade WHERE est_cod = " + estado.getCodigo();
        ResultSet rs = Banco.getCon().consultar(sql);
        ObservableList<Cidade> list = FXCollections.observableArrayList();
        
        try
        {
            while(rs.next())
            {
                codigo = rs.getInt("cid_cod");
                estado = DALEstado.searchByCodigo(rs.getInt("est_cod"));
                nome = rs.getString("cid_nome");
                
                
                list.add(new Cidade(codigo, estado, nome));
            }
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
        
        return list;
    }
    
    public Cidade searchByCodigo()
    {
        String sql = "SELECT * FROM cidade WHERE cid_cod = " + codigo;
        ResultSet rs = Banco.getCon().consultar(sql);
        Cidade list = null;
        
        try
        {
            while(rs.next())
            {
                codigo = rs.getInt("cid_cod");
                estado = DALEstado.searchByCodigo(rs.getInt("est_cod"));
                nome = rs.getString("cid_nome");
                
                
                list = new Cidade(codigo, estado, nome);
            }
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
        
        return list;
    }
}
