package Class;

import DAL.DALFuncionario;
import DAL.DALMesa;
import DataBase.Banco;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Date;
import java.sql.ResultSet;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Comanda
{
    private int codigo;
    private String nome;
    private String status;
    private Date data;
    private double total;
    private Mesa mesa;
    private Funcionario func;

    public Comanda()
    {
        
    }

    public Comanda(int codigo)
    {
        this.codigo = codigo;
    }

    public Comanda(String nome, String status, Date data, Funcionario func)
    {
        this.nome = nome;
        this.status = status;
        this.data = data;
        this.func = func;
    }

    public Comanda(String nome, String status, Date data, Mesa mesa, Funcionario func)
    {
        this.nome = nome;
        this.status = status;
        this.data = data;
        this.mesa = mesa;
        this.func = func;
    }

    public Comanda(int codigo, String nome, String status, Date data, double total, Mesa mesa, Funcionario func) 
    {
        this.codigo = codigo;
        this.nome = nome;
        this.status = status;
        this.data = data;
        this.total = total;
        this.mesa = mesa;
        this.func = func;
    }

    
    
    public int getCodigo()
    {
        return codigo;
    }

    public void setCodigo(int codigo)
    {
        this.codigo = codigo;
    }

    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public Date getData()
    {
        return data;
    }

    public void setData(Date data)
    {
        this.data = data;
    }

    public double getTotal()
    {
        return total;
    }

    public void setTotal(double total)
    {
        this.total = total;
    }

    public Mesa getMesa()
    {
        return mesa;
    }

    public void setMesa(Mesa mesa)
    {
        this.mesa = mesa;
    }

    public Funcionario getFunc()
    {
        return func;
    }

    public void setFunc(Funcionario func)
    {
        this.func = func;
    }

    @Override
    public String toString()
    {
        return codigo + " - " + nome;
    }
    
    
    
    public void insert() throws SQLException
    {
        String sql;
        
        if(mesa != null)
        {
            sql = "INSERT INTO comanda(com_nome, com_status, com_data, com_total, mesa_cod, func_cod) VALUES (?, ?, ?, ?, ?, ?)";
            
            Connection connection = Banco.getCon().getConnection();
            PreparedStatement statement = connection.prepareStatement(sql);

            statement.setString(1, nome);
            statement.setString(2, "A");
            statement.setDate(3, data);
            statement.setDouble(4, 0);
            statement.setInt(5, mesa.getCodigo());
            statement.setInt(6, func.getCodigo());
            
            statement.execute();
        }
        else
        {
            sql = "INSERT INTO comanda(com_nome, com_status, com_data, com_total, func_cod) VALUES (?, ?, ?, ?, ?)";
            
            Connection connection = Banco.getCon().getConnection();
            PreparedStatement statement = connection.prepareStatement(sql);

            statement.setString(1, nome);
            statement.setString(2, "A");
            statement.setDate(3, data);
            statement.setDouble(4, 0);
            statement.setInt(5, func.getCodigo());
            
            statement.execute();
        }
    }
    
    public void close() throws SQLException
    {
        String sql;
       
        sql = "UPDATE comanda SET com_status = 'F' WHERE com_cod = " + codigo;

        Banco.getCon().manipular(sql);

    }
    
    public ObservableList<Comanda> searchByMesa()
    {
        String sql = "SELECT * FROM comanda WHERE com_status = 'A' AND mesa_cod = " + mesa.getCodigo();
        ResultSet rs = Banco.getCon().consultar(sql);
        ObservableList<Comanda> list = FXCollections.observableArrayList();
        
        try
        {
            while(rs.next())
            {
                codigo = rs.getInt("com_cod");
                nome = rs.getString("com_nome");
                status = rs.getString("com_status");
                data = rs.getDate("com_data");
                total = rs.getDouble("com_total");
                mesa = rs.getInt("mesa_cod") != 0 ? DALMesa.searchByCodigo(rs.getInt("mesa_cod")) : null;
                func = rs.getInt("func_cod") != 0 ? DALFuncionario.searchByCodigo(rs.getInt("func_cod")) : null;
                
                list.add(new Comanda(codigo, nome, status, data, total, mesa, func));
            }
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
        
        return list;
    }
    
    public ObservableList<Comanda> searchAll()
    {
        return null;
    }
}
