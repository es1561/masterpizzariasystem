package Class;

import DAL.DALCidade;
import DataBase.Banco;
import Main.Main;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Produto
{
    private int codigo;
    private String nome;
    private String descricao;
    private double valor;
    private char tipo;

    public Produto()
    {
        
    }

    public Produto(int codigo)
    {
        this.codigo = codigo;
    }

    public Produto(String nome, String descricao, double valor, char tipo)
    {
        this.nome = nome;
        this.descricao = descricao;
        this.valor = valor;
        this.tipo = tipo;
    }

    public Produto(int codigo, String nome, String descricao, double valor, char tipo)
    {
        this.codigo = codigo;
        this.nome = nome;
        this.descricao = descricao;
        this.valor = valor;
        this.tipo = tipo;
    }

    public int getCodigo()
    {
        return codigo;
    }

    public void setCodigo(int codigo)
    {
        this.codigo = codigo;
    }

    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

    public String getDescricao()
    {
        return descricao;
    }

    public void setDescricao(String descricao)
    {
        this.descricao = descricao;
    }

    public double getValor()
    {
        return valor;
    }

    public void setValor(double valor)
    {
        this.valor = valor;
    }

    public char getTipo()
    {
        return tipo;
    }

    public void setTipo(char tipo)
    {
        this.tipo = tipo;
    }
    
    public void insert() throws SQLException
    {
        String sql = "INSERT INTO produto(prod_cod, prod_nome, prod_desc, prod_tipo, prod_valor) ";
        String values = "VALUES(?, ?, ?, ?, ?)";
        
        Connection connection = Banco.getCon().getConnection();
        PreparedStatement statement = connection.prepareStatement(sql + values);

        statement.setInt(1, Banco.getCon().getMaxPK("produto", "prod_cod") + 1);
        statement.setString(2, nome);
        statement.setString(3, descricao);
        statement.setString(4, "" + tipo);
        statement.setDouble(5, valor);
        
        statement.execute();
    }
    
    public void update() throws SQLException
    {
        String sql = "UPDATE produto SET prod_nome = ?, prod_desc = ?, prod_tipo = ?, prod_valor = ? WHERE func_cod = " + codigo;

        Connection connection = Banco.getCon().getConnection();
        PreparedStatement statement = connection.prepareStatement(sql);

        statement.setString(1, nome);
        statement.setString(2, descricao);
        statement.setString(3, "" + tipo);
        statement.setDouble(4, valor);

        statement.execute();
    }
    
    public boolean delete() throws SQLException
    {
        String sql = "DELETE FROM produto WHERE prod_cod = " + codigo;

        try
        {
            Banco.getCon().getConnection().setAutoCommit(false);
            
            //deletar estoque;
            
            Banco.getCon().manipular(sql);
            Banco.getCon().getConnection().commit();
            Banco.getCon().getConnection().setAutoCommit(true);
            
            return true;
        }
        catch(Exception ex)
        {
           Banco.getCon().getConnection().rollback();
           Banco.getCon().getConnection().setAutoCommit(true);
           
           return false;
        }
    }
    
    public ObservableList<Produto> searchByNome()
    {
        String sql = "SELECT * FROM produto WHERE UPPER(func_nome) LIKE UPPER('" + nome + "%')";
        ResultSet rs = Banco.getCon().consultar(sql);
        ObservableList<Produto> list = FXCollections.observableArrayList();
        
        try
        {
            while(rs.next())
            {
                codigo = rs.getInt("prod_cod");
                nome = rs.getString("prod_nome");
                descricao = rs.getString("prod_desc");
                tipo = rs.getString("prod_tipo").charAt(0);
                
                list.add(new Produto(codigo, nome, descricao, valor, tipo));
            }
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
        
        return list;
    }
    
    public ObservableList<Produto> searchByTipo()
    {
        String sql = "SELECT * FROM produto WHERE prod_tipo = '" + tipo + "'";
        ResultSet rs = Banco.getCon().consultar(sql);
        ObservableList<Produto> list = FXCollections.observableArrayList();
        
        try
        {
            while(rs.next())
            {
                codigo = rs.getInt("prod_cod");
                nome = rs.getString("prod_nome");
                descricao = rs.getString("prod_desc");
                tipo = rs.getString("prod_tipo").charAt(0);
                
                list.add(new Produto(codigo, nome, descricao, valor, tipo));
            }
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
        
        return list;
    }
    
    public ObservableList<Produto> searchAll()
    {
        String sql = "SELECT * FROM produto";
        ResultSet rs = Banco.getCon().consultar(sql);
        ObservableList<Produto> list = FXCollections.observableArrayList();
        
        try
        {
            while(rs.next())
            {
                codigo = rs.getInt("prod_cod");
                nome = rs.getString("prod_nome");
                descricao = rs.getString("prod_desc");
                tipo = rs.getString("prod_tipo").charAt(0);
                
                list.add(new Produto(codigo, nome, descricao, valor, tipo));
            }
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
        
        return list;
    }
}
