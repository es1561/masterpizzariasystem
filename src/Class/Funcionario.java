package Class;

import DAL.DALCidade;
import DAL.DALEstado;
import Main.Main;
import DataBase.Banco;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Funcionario 
{
    private int codigo;
    private String nome;
    private String cpf;
    private String fone;
    private String email;
    private String login;
    private String senha;
    private int nivel;
    private boolean ativo;
    private Cidade cidade;
    private String logradouro;

    public Funcionario()
    {
        
    }

    public Funcionario(int codigo)
    {
        this.codigo = codigo;
    }
    
    public Funcionario(String nome, String cpf, String fone, String email, String login, String senha, int nivel, boolean ativo)
    {
        this.nome = nome;
        this.cpf = cpf;
        this.fone = fone;
        this.email = email;
        this.login = login;
        this.senha = senha;
        this.nivel = nivel;
        this.ativo = ativo;
    }
    
    public Funcionario(int codigo, String nome, String cpf, String fone, String email, String login, String senha, int nivel, boolean ativo)
    {
        this.codigo = codigo;
        this.nome = nome;
        this.cpf = cpf;
        this.fone = fone;
        this.email = email;
        this.login = login;
        this.senha = senha;
        this.nivel = nivel;
        this.ativo = ativo;
    }

    public Funcionario(int codigo, String nome, String cpf, String fone, String email, String login, String senha, int nivel, boolean ativo, Cidade cidade, String logradouro)
    {
        this.codigo = codigo;
        this.nome = nome;
        this.cpf = cpf;
        this.fone = fone;
        this.email = email;
        this.login = login;
        this.senha = senha;
        this.nivel = nivel;
        this.ativo = ativo;
        this.cidade = cidade;
        this.logradouro = logradouro;
    }
    
    public int getCodigo()
    {
        return codigo;
    }

    public void setCodigo(int codigo)
    {
        this.codigo = codigo;
    }

    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

    public String getCpf()
    {
        return cpf;
    }

    public void setCpf(String cpf)
    {
        this.cpf = cpf;
    }

    public String getFone()
    {
        return fone;
    }

    public void setFone(String fone)
    {
        this.fone = fone;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getLogin()
    {
        return login;
    }

    public void setLogin(String login)
    {
        this.login = login;
    }

    public String getSenha()
    {
        return senha;
    }

    public void setSenha(String senha)
    {
        this.senha = senha;
    }

    public int getNivel()
    {
        return nivel;
    }

    public void setNivel(int nivel)
    {
        this.nivel = nivel;
    }

    public boolean isAtivo()
    {
        return ativo;
    }

    public void setAtivo(boolean ativo)
    {
        this.ativo = ativo;
    }

    public Cidade getCidade()
    {
        return cidade;
    }

    public void setCidade(Cidade cidade)
    {
        this.cidade = cidade;
    }

    public String getLogradouro()
    {
        return logradouro;
    }

    public void setLogradouro(String logradouro)
    {
        this.logradouro = logradouro;
    }
    
    public void insert() throws SQLException
    {
        String sql = "INSERT INTO Funcionario(func_cod, func_nome, func_cpf, func_fone, func_email, func_login, func_senha, func_nivel, func_ativo, func_cidade, func_logradouro) ";
        String values = "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        
        Connection connection = Banco.getCon().getConnection();
        PreparedStatement statement = connection.prepareStatement(sql + values);

        statement.setInt(1, Banco.getCon().getMaxPK("funcionario", "func_cod") + 1);
        statement.setString(2, nome);
        statement.setString(3, cpf);
        statement.setString(4, fone);
        statement.setString(5, email);
        statement.setString(6, login);
        statement.setString(7, senha);
        statement.setInt(8, nivel);
        statement.setString(9, ativo ? "T" : "F");
        statement.setInt(10, cidade.getCodigo());
        statement.setString(11, logradouro);
        
        statement.execute();
    }
    
    public void update() throws SQLException
    {
        if(!(nivel == 2 && !ativo && countADM() == 1))
        {
            String sql = "UPDATE Funcionario SET func_nome = ?, func_cpf = ?, func_fone = ?, func_email = ?, func_login = ?, func_senha = ?, func_nivel = ?, func_ativo = ?, func_cidade = ?, func_logradouro = ? WHERE func_cod = " + codigo;

            Connection connection = Banco.getCon().getConnection();
            PreparedStatement statement = connection.prepareStatement(sql);

            statement.setString(1, nome);
            statement.setString(2, cpf);
            statement.setString(3, fone);
            statement.setString(4, email);
            statement.setString(5, login);
            statement.setString(6, senha);
            statement.setInt(7, nivel);
            statement.setString(8, ativo ? "V" : "F");
            statement.setInt(9, cidade.getCodigo());
            statement.setString(10, logradouro);

            statement.execute();
        }
    }
    
    public boolean delete()
    {
        Funcionario func = searchByCodigo();
        
        if(func.isAtivo() && func.getNivel() == 2 && countADM() == 1)
            return false;
        
        String sql = "DELETE FROM funcionario WHERE func_cod = " + codigo + " AND func_cod != " + Main._user.getCodigo();

        return Banco.getCon().manipular(sql);
    }
    
    public boolean login()
    {
        boolean flag;
        String sql = "SELECT * FROM funcionario WHERE func_login = '" + login + "' AND func_senha = '" + senha + "' AND func_ativo = 'T'";
        ResultSet rs = Banco.getCon().consultar(sql);
        
        try
        {
            if(rs.next())
            {
                codigo = rs.getInt("func_cod");
                nome = rs.getString("func_nome");
                cpf = rs.getString("func_cpf");
                fone = rs.getString("func_fone");
                email = rs.getString("func_email");
                nivel = rs.getInt("func_nivel");
                ativo = rs.getString("func_ativo").compareTo("T") == 0;
                cidade = DALCidade.searchByCodigo(rs.getInt("func_cidade"));
                logradouro = rs.getString("func_logradouro");
                
                flag = true;
            }
            else
                flag = false;
        }
        catch(Exception ex)
        {
            flag = false;
        }
        
        return flag;
    }
    
    private int countADM()
    {
        int count = 0;
        String sql = "SELECT COUNT(*) as N FROM funcionario WHERE func_nivel = 2 AND func_ativo = 'T'";
        ResultSet rs = Banco.getCon().consultar(sql);
        
        try
        {
            if(rs.next())
                count = rs.getInt("N");
        }
        catch(Exception ex)
        {
            
        }
        
        return count;
    }
    
    public ObservableList<Funcionario> searchAll()
    {
        String sql = "SELECT * FROM funcionario";
        ResultSet rs = Banco.getCon().consultar(sql);
        ObservableList<Funcionario> list = FXCollections.observableArrayList();
        
        try
        {
            while(rs.next())
            {
                codigo = rs.getInt("func_cod");
                nome = rs.getString("func_nome");
                cpf = rs.getString("func_cpf");
                fone = rs.getString("func_fone");
                email = rs.getString("func_email");
                login = rs.getString("func_login");
                senha = rs.getString("func_senha");
                nivel = rs.getInt("func_nivel");
                ativo = rs.getString("func_ativo").compareTo("T") == 0;
                cidade = DALCidade.searchByCodigo(rs.getInt("func_cidade"));
                logradouro = rs.getString("func_logradouro");
                
                list.add(new Funcionario(codigo, nome, cpf, fone, email, login, senha, nivel, ativo, cidade, logradouro));
            }
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
        
        return list;
    }
    
    public Funcionario searchByCodigo()
    {
        String sql = "SELECT * FROM funcionario WHERE func_cod = " + codigo;
        ResultSet rs = Banco.getCon().consultar(sql);
        Funcionario list = null;
        
        try
        {
            while(rs.next())
            {
                codigo = rs.getInt("func_cod");
                nome = rs.getString("func_nome");
                cpf = rs.getString("func_cpf");
                fone = rs.getString("func_fone");
                email = rs.getString("func_email");
                login = rs.getString("func_login");
                senha = rs.getString("func_senha");
                nivel = rs.getInt("func_nivel");
                ativo = rs.getString("func_ativo").compareTo("T") == 0;
                cidade = DALCidade.searchByCodigo(rs.getInt("func_cidade"));
                logradouro = rs.getString("func_logradouro");
                
                list= new Funcionario(codigo, nome, cpf, fone, email, login, senha, nivel, ativo, cidade, logradouro);
            }
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
        
        return list;
    }
    
    public ObservableList<Funcionario> searchByName()
    {
        String sql = "SELECT * FROM funcionario WHERE UPPER(func_nome) LIKE UPPER('" + nome + "%')";
        ResultSet rs = Banco.getCon().consultar(sql);
        ObservableList<Funcionario> list = FXCollections.observableArrayList();
        
        try
        {
            while(rs.next())
            {
                codigo = rs.getInt("func_cod");
                nome = rs.getString("func_nome");
                cpf = rs.getString("func_cpf");
                fone = rs.getString("func_fone");
                email = rs.getString("func_email");
                login = rs.getString("func_login");
                senha = rs.getString("func_senha");
                nivel = rs.getInt("func_nivel");
                ativo = rs.getString("func_ativo").compareTo("T") == 0;
                cidade = DALCidade.searchByCodigo(rs.getInt("func_cidade"));
                logradouro = rs.getString("func_logradouro");
                
                list.add(new Funcionario(codigo, nome, cpf, fone, email, login, senha, nivel, ativo, cidade, logradouro));
            }
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
        
        return list;
    }
    
    public ObservableList<Funcionario> searchByCPF()
    {
        String sql = "SELECT * FROM funcionario WHERE func_cpf LIKE '" + cpf + "%'";
        ResultSet rs = Banco.getCon().consultar(sql);
        ObservableList<Funcionario> list = FXCollections.observableArrayList();
        
        try
        {
            while(rs.next())
            {
                codigo = rs.getInt("func_cod");
                nome = rs.getString("func_nome");
                cpf = rs.getString("func_cpf");
                fone = rs.getString("func_fone");
                email = rs.getString("func_email");
                login = rs.getString("func_login");
                senha = rs.getString("func_senha");
                nivel = rs.getInt("func_nivel");
                ativo = rs.getString("func_ativo").compareTo("T") == 0;
                cidade = DALCidade.searchByCodigo(rs.getInt("func_cidade"));
                logradouro = rs.getString("func_logradouro");
                
                list.add(new Funcionario(codigo, nome, cpf, fone, email, login, senha, nivel, ativo, cidade, logradouro));
            }
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
        
        return list;
    }
    
    public ObservableList<Funcionario> searchByActive()
    {
        String sql = "SELECT * FROM funcionario WHERE func_ativo = " + (ativo ? "'T'" : "'F'");
        ResultSet rs = Banco.getCon().consultar(sql);
        ObservableList<Funcionario> list = FXCollections.observableArrayList();
        
        try
        {
            while(rs.next())
            {
                codigo = rs.getInt("func_cod");
                nome = rs.getString("func_nome");
                cpf = rs.getString("func_cpf");
                fone = rs.getString("func_fone");
                email = rs.getString("func_email");
                login = rs.getString("func_login");
                senha = rs.getString("func_senha");
                nivel = rs.getInt("func_nivel");
                ativo = rs.getString("func_ativo").compareTo("T") == 0;
                cidade = DALCidade.searchByCodigo(rs.getInt("func_cidade"));
                logradouro = rs.getString("func_logradouro");
                
                list.add(new Funcionario(codigo, nome, cpf, fone, email, login, senha, nivel, ativo, cidade, logradouro));
            }
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
        
        return list;
    }
    
    public boolean uniqueLogin(boolean not_me)
    {
        int count;
        String sql;
        ResultSet rs;
        
        if(not_me)
            sql = "SELECT COUNT(*) as N FROM funcionario WHERE func_login = '" + login + "' AND func_cod != " + codigo;
        else
            sql = "SELECT COUNT(*) as N FROM funcionario WHERE func_login = '" + login + "'";
        
        rs = Banco.getCon().consultar(sql);
        
        try
        {
            if(rs.next())
                count = rs.getInt("N");
            else
                count = 0;
        }
        catch(Exception ex)
        {
            count = 0;
        }
        
        return count < 1;
    }
    
    public boolean uniqueCPF(boolean not_me)
    {
        int count;
        String sql;
        ResultSet rs;

        if(not_me)
            sql = "SELECT COUNT(*) as N FROM funcionario WHERE func_cpf = '" + cpf + "' AND func_cod != " + codigo;
        else
            sql = "SELECT COUNT(*) as N FROM funcionario WHERE func_cpf = '" + cpf + "'";
        
        rs = Banco.getCon().consultar(sql);
        
        try
        {
            if(rs.next())
                count = rs.getInt("N");
            else
                count = 0;
        }
        catch(Exception ex)
        {
            count = 0;
        }
        
        return count < 1;
    }
}
