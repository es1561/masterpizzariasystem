package Class;

import DataBase.Banco;
import java.sql.ResultSet;
import javafx.scene.image.Image;

public class Sistema
{
    private static String nome_empresa;
    private static String nome_fantasia;
    private static String razao_social;
    private static String fone;
    private static Image logo_grande;
    private static Image logo_mini;
    private static String cor_fundo;
    private static String cor_frente;
    private static String cor_botao;
    
    private Sistema()
    {
        
    }

    public static boolean reload()
    {
        //carregar do banco para o objeto...
        ResultSet rs = Banco.getCon().consultar("SELECT * FROM sistema");
        
        try
        {
            if(rs.next())
            {
                nome_empresa = rs.getString("nome_empresa");
                nome_fantasia = rs.getString("nome_fantasia");
                razao_social = rs.getString("razao_social");
                fone = rs.getString("fone");
                cor_fundo = rs.getString("cor_fundo");
                cor_frente = rs.getString("cor_frente");
                cor_botao = rs.getString("cor_botao");
            }
            else
                return false;
            
            return true;
        }
        catch(Exception ex)
        {
            return false;
        }
    }
    
    public static String getNome_empresa()
    {
        return nome_empresa;
    }

    public static String getNome_fantasia()
    {
        return nome_fantasia;
    }

    public static String getRazao_social()
    {
        return razao_social;
    }

    public static String getFone()
    {
        return fone;
    }

    public static Image getLogo_grande()
    {
        return logo_grande;
    }

    public static Image getLogo_mini()
    {
        return logo_mini;
    }

    public static String getCor_fundo()
    {
        return cor_fundo;
    }

    public static String getCor_frente()
    {
        return cor_frente;
    }

    public static String getCor_botao()
    {
        return cor_botao;
    }
}
