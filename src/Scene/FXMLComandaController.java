package Scene;

import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableView;

public class FXMLComandaController implements Initializable
{
    @FXML
    private ListView<?> list_comanda;
    @FXML
    private JFXButton btn_new;
    @FXML
    private JFXButton btn_fechar_mesa;
    @FXML
    private Label lb_total;
    @FXML
    private JFXButton btn_pagar;
    @FXML
    private Label lb_comanda;
    @FXML
    private Label lb_subtotal;
    @FXML
    private JFXButton btn_fechar_comanda;
    @FXML
    private TableView<?> table_item;
    @FXML
    private JFXButton btn_add;
    @FXML
    private JFXButton btn_remove;

    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        // TODO
    }    

    @FXML
    private void ClickNew(ActionEvent event)
    {
        
    }

    @FXML
    private void ClickFecharMesa(ActionEvent event)
    {
        
    }

    @FXML
    private void ClickPagar(ActionEvent event)
    {
        
    }

    @FXML
    private void ClickFecharComanda(ActionEvent event)
    {
        
    }

    @FXML
    private void ClickADD(ActionEvent event)
    {
        
    }

    @FXML
    private void ClickRemove(ActionEvent event)
    {
        
    }
    
}
