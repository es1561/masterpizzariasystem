package Scene;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 * @author Matheus V.;
 */
public class FXMLRegistrarRecebimentoController implements Initializable {

    @FXML
    private JFXComboBox<?> cb_formaPagamento;
    @FXML
    private JFXTextField tf_desconto;
    @FXML
    private JFXTextField tf_acresc;
    @FXML
    private Label lb_total;
    @FXML
    private Label lb_pago;
    @FXML
    private Label lb_troco;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void clk_confirmar(ActionEvent event) {
    }
    
}
