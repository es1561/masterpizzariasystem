package Scene;

import Class.Funcionario;
import Class.Sistema;
import Main.Main;
import DAL.DALFuncionario;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javax.swing.JOptionPane;

public class FXMLLoginController implements Initializable
{
    public static Funcionario user;
    @FXML
    private AnchorPane ap_fundo;
    @FXML
    private TextField tb_login;
    @FXML
    private PasswordField tb_senha;
    @FXML
    private JFXButton btn_login;
    @FXML
    private JFXButton btn_sair;
    @FXML
    private ImageView img_logo;
    
    private void load()
    {
        //fundo
        ap_fundo.setStyle("-fx-background-color:" + Sistema.getCor_fundo());
        img_logo.setImage(Sistema.getLogo_grande());
        
        //botoes
        btn_login.setStyle("-fx-background-color:" + Sistema.getCor_botao());
        btn_sair.setStyle("-fx-background-color:" + Sistema.getCor_botao());
    }
    
    private void close()
    {
        System.exit(0);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        // TODO
        load();
    }    

    @FXML
    private void ClickLogin(ActionEvent event)
    {
        user = DALFuncionario.login(tb_login.getText(), tb_senha.getText());
        
        if(user == null)
        {
            new Alert(Alert.AlertType.ERROR, "Login e/ou Senha Incorreto(s)", ButtonType.OK).showAndWait();
            tb_senha.clear();
        }
        else
        {
            Main._user = user;
            tb_login.getScene().getWindow().hide();
        }
    }

    @FXML
    private void ClickSair(ActionEvent event)
    {
        close();
    }
    
}
