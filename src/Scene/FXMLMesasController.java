package Scene;

import Class.Comanda;
import Class.Funcionario;
import Class.Mesa;
import Class.Sistema;
import DAL.DALComanda;
import DAL.DALMesa;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

public class FXMLMesasController implements Initializable
{
    private Mesa select;
    private Comanda com_select;
    
    @FXML
    private JFXButton btn_mesa;
    @FXML
    private TableView<Mesa> table_mesas;
    @FXML
    private ListView<Comanda> list_comanda;
    @FXML
    private JFXButton btn_com_novo;
    @FXML
    private JFXButton btn_com_fechar;
    @FXML
    private ListView<?> list_item;
    @FXML
    private JFXButton btn_item_novo;
    @FXML
    private JFXButton btn_item_remover;
    @FXML
    private Label lb_mesa;
    @FXML
    private Label lb_comanda;
    @FXML
    private TableColumn<Mesa, Integer> c_num;
    @FXML
    private TableColumn<Mesa, String> c_status;
    @FXML
    private VBox vb_back_1;
    @FXML
    private VBox vb_back_2;

    private void lockAll()
    {
        btn_mesa.setDisable(true);
        disableComanda(true);
        disableItem(true);
    }
    
    private void disableComanda(boolean flag)
    {
        list_comanda.setDisable(flag);
        btn_com_novo.setDisable(flag);
        btn_com_fechar.setDisable(flag);
    }
    
    private void disableItem(boolean flag)
    {
        list_item.setDisable(flag);
        btn_item_novo.setDisable(flag);
        btn_item_remover.setDisable(flag);
    }
    
    private void refreshComanda()
    {
        list_comanda.setItems(DALComanda.searchByMesa(select.getCodigo()));
    }
    
    private void newComanda() throws SQLException
    {
        TextInputDialog dialog = new TextInputDialog("Cli " + select.getCodigo());
        dialog.setTitle("Nova Comanda");
        dialog.setHeaderText("Insira o nome da Comanda");
        dialog.setContentText("Nome:");

        Optional<String> result = dialog.showAndWait();

        if(result.isPresent() && DALMesa.update(select.getCodigo(), "O"))
        {
            DALComanda.insert(result.get(), select.getCodigo());
            refreshComanda();
            disableComanda(false);
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        // TODO
        
        c_num.setCellValueFactory(new PropertyValueFactory<Mesa, Integer>("codigo"));
        c_status.setCellValueFactory(new PropertyValueFactory<Mesa, String>("status"));
        
        btn_com_novo.setStyle("-fx-background-color:" + Sistema.getCor_botao());
        btn_com_fechar.setStyle("-fx-background-color:" + Sistema.getCor_botao());
        btn_item_novo.setStyle("-fx-background-color:" + Sistema.getCor_botao());
        btn_item_remover.setStyle("-fx-background-color:" + Sistema.getCor_botao());
        btn_mesa.setStyle("-fx-background-color:" + Sistema.getCor_botao());
        
        vb_back_1.setStyle("-fx-background-color:" + Sistema.getCor_fundo());
        vb_back_2.setStyle("-fx-background-color:" + Sistema.getCor_fundo());

        table_mesas.setItems(DALMesa.searchAll());
        
        lockAll();
    }    

    @FXML
    private void ClickMesa(ActionEvent event) throws SQLException
    {
        if(btn_mesa.getText().compareTo("Abrir") == 0)
        {
            newComanda();
        }
        else
        {
            ObservableList<Comanda> list = DALComanda.searchByMesa(select.getCodigo());
            
            for (Comanda comanda : list) 
                comanda.close();
            
            DALMesa.update(select.getCodigo(), "L");
            list_comanda.setItems(null);
            disableComanda(true);
        }
        
        btn_mesa.setText("Action");
        btn_mesa.setDisable(true);
        table_mesas.setItems(DALMesa.searchAll());
    }

    @FXML
    private void ClickListComanda(MouseEvent event)
    {
        if(list_comanda.getSelectionModel().getSelectedIndex() >= 0)
        {
            com_select = list_comanda.getSelectionModel().getSelectedItem();
            btn_com_fechar.setDisable(false);
        }
    }

    @FXML
    private void ClickNovaComanda(ActionEvent event) throws SQLException
    {
        newComanda();
    }

    @FXML
    private void ClickFecharComanda(ActionEvent event) throws SQLException
    {
        if(com_select != null)
        {
            DALComanda.close(com_select.getCodigo());
            refreshComanda();
        }
    }

    @FXML
    private void ClickListItem(MouseEvent event)
    {
        
    }

    @FXML
    private void ClickNovoItem(ActionEvent event)
    {
        
    }

    @FXML
    private void ClickRemoverItem(ActionEvent event)
    {
        
    }

    @FXML
    private void ClickListMesa(MouseEvent event)
    {
        if(table_mesas.getSelectionModel().getSelectedIndex() >= 0)
        {
            boolean flag;
            
            select = table_mesas.getSelectionModel().getSelectedItem();

            flag = select.getStatus().compareTo("L") == 0;
            
            if(!flag)
                refreshComanda();
            else
                list_comanda.setItems(null);
                
            list_comanda.setDisable(flag);
            btn_com_novo.setDisable(flag);
            btn_com_fechar.setDisable(true);
            
            btn_mesa.setDisable(false);
            btn_mesa.setText(flag ? "Abrir" : "Fechar");
        }
    }
    
}
