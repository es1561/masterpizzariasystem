package Scene;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

/**
 * FXML Controller class
 *
 * @author Matheus V.;
 */
public class FXMLRegistrarVendaController implements Initializable {

    @FXML
    private TableView<?> tabItens;
    @FXML
    private TableColumn<?, ?> col_nome;
    @FXML
    private TableColumn<?, ?> col_qtd;
    @FXML
    private TableColumn<?, ?> col_precoUn;
    @FXML
    private TableColumn<?, ?> col_precoTot;
    @FXML
    private Label lb_total;
    @FXML
    private Label lb_pago;
    @FXML
    private Label lb_troco;
    @FXML
    private Label lb_data;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void clk_addItem(ActionEvent event) {
    }

    @FXML
    private void clk_retirarItem(ActionEvent event) {
    }

    @FXML
    private void clk_realizarPagamento(ActionEvent event) {
    }

    @FXML
    private void clk_confVenda(ActionEvent event) {
    }
    
}
