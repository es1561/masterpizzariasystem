package Scene;

import Class.Cidade;
import Class.Estado;
import Class.Funcionario;
import Class.Sistema;
import Class.MaskFieldUtil;
import DAL.DALCidade;
import DAL.DALEstado;
import DAL.DALFuncionario;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javax.swing.JOptionPane;


public class FXMLFuncionarioController implements Initializable
{
    private Funcionario select;
    private String error;
    
    @FXML
    private HBox hb_front_1;
    @FXML
    private JFXButton btn_novo;
    @FXML
    private JFXButton btn_editar;
    @FXML
    private JFXButton btn_apagar;
    @FXML
    private JFXButton btn_sair;
    @FXML
    private VBox vb_back;
    @FXML
    private JFXTextField tb_nome;
    @FXML
    private JFXTextField tb_cpf;
    @FXML
    private JFXTextField tb_login;
    @FXML
    private JFXTextField tb_senha;
    @FXML
    private JFXTextField tb_email;
    @FXML
    private JFXTextField tb_fone;
    @FXML
    private ComboBox<String> cb_nivel;
    @FXML
    private CheckBox check_ativo;
    @FXML
    private HBox hn_front_2;
    @FXML
    private ComboBox<String> cb_filtro;
    @FXML
    private TextField tb_buscar;
    @FXML
    private JFXButton btn_buscar;
    @FXML
    private TableView<Funcionario> table_func;
    @FXML
    private TableColumn<Funcionario, Integer> c_cod;
    @FXML
    private TableColumn<Funcionario, String> c_nome;
    @FXML
    private TableColumn<Funcionario, String> c_cpf;
    @FXML
    private TableColumn<Funcionario, String> c_fone;
    @FXML
    private TableColumn<Funcionario, String> c_email;
    @FXML
    private TableColumn<Funcionario, Integer> c_nivel;
    @FXML
    private TableColumn<Funcionario, String> c_login;
    @FXML
    private TableColumn<Funcionario, Boolean> c_ativo;
    @FXML
    private ComboBox<Estado> cb_estado;
    @FXML
    private ComboBox<Cidade> cb_cidade;
    @FXML
    private JFXTextField tb_local;
    
    private void load()
    {
        //scene
        vb_back.setStyle("-fx-background-color:" + Sistema.getCor_fundo());
        hb_front_1.setStyle("-fx-background-color:" + Sistema.getCor_frente());
        hb_front_1.setStyle("-fx-background-color:" + Sistema.getCor_frente());
        
        //botoes
        btn_apagar.setStyle("-fx-background-color:" + Sistema.getCor_botao());
        btn_buscar.setStyle("-fx-background-color:" + Sistema.getCor_botao());
        btn_editar.setStyle("-fx-background-color:" + Sistema.getCor_botao());
        btn_novo.setStyle("-fx-background-color:" + Sistema.getCor_botao());
        btn_sair.setStyle("-fx-background-color:" + Sistema.getCor_botao());
    }
    
    private void refresh()
    {
        btn_novo.setDisable(false);
        btn_editar.setDisable(true);
        btn_apagar.setDisable(true);
        
        btn_novo.setText("Novo");
        btn_editar.setText("Editar");
        btn_sair.setText("Sair");
        
        disableField(true);
        clearFields();
        
        cb_nivel.setDisable(true);
        cb_nivel.setValue(cb_nivel.getItems().get(0));
        check_ativo.setDisable(true);
        
        cb_estado.setValue(cb_estado.getItems().get(0));
        loadCidade();
        cb_cidade.setValue(cb_cidade.getItems().get(0));
        
        disableSearch(false);
        cb_filtro.setValue(cb_filtro.getItems().get(0));
        table_func.setItems(null);
    }
    
    private void clearFields()
    {
        tb_nome.clear();
        tb_cpf.clear();
        tb_fone.clear();
        tb_email.clear();
        tb_login.clear();
        tb_senha.clear();
    }
    
    private void loadFields()
    {
        tb_nome.setText(select.getNome());
        tb_cpf.setText(select.getCpf());
        tb_fone.setText(select.getFone());
        tb_email.setText(select.getEmail());
        tb_login.setText(select.getLogin());
        tb_senha.setText(select.getSenha());
        cb_nivel.setValue(cb_nivel.getItems().get(select.getNivel()));
        check_ativo.setSelected(select.isAtivo());
        cb_estado.setValue(select.getCidade().getEstado());
        loadCidade();
        cb_cidade.setValue(select.getCidade());
        tb_local.setText(select.getLogradouro());
    }
    
    private boolean checkFields()
    {
        boolean result = true;
        error = "Campos Invalidos:\n\n";
        
        if(tb_nome.getText().isEmpty())
        {
            tb_nome.setStyle("-fx-text-fill: red");
            error += "Nome[Empety]\n";
            result = false;
        }
        else
            tb_nome.setStyle("-fx-text-fill: white");
        
        if(tb_cpf.getText().isEmpty())
        {
            if(select != null && !DALFuncionario.uniqueCPF(select.getCodigo(), tb_cpf.getText(), true))
            {
                tb_cpf.setStyle("-fx-text-fill: red");
                error += tb_cpf.getText().isEmpty() ? "CPF[Empety]\n" : "CPF[Invalid]\n";
                result = false;
            }
            else if(!DALFuncionario.uniqueCPF(0, tb_cpf.getText(), false))
            {
                tb_cpf.setStyle("-fx-text-fill: red");
                error += tb_cpf.getText().isEmpty() ? "CPF[Empety]\n" : "CPF[Invalid]\n";
                result = false;
            }
        }
        else
            tb_cpf.setStyle("-fx-text-fill: white");
        
        if(tb_login.getText().isEmpty())
        {
            if(select != null && !DALFuncionario.uniqueLogin(select.getCodigo(), tb_login.getText(), true))
            {
                tb_login.setStyle("-fx-text-fill: red");
                error += tb_login.getText().isEmpty() ? "Login[Empety]\n" : "Login[Invalid]\n";
                result = false;
            }
            else if(!DALFuncionario.uniqueLogin(0, tb_login.getText(), false))
            {
                tb_login.setStyle("-fx-text-fill: red");
                error += tb_login.getText().isEmpty() ? "Login[Empety]\n" : "Login[Invalid]\n";
                result = false;
            }
        }
        else
            tb_login.setStyle("-fx-text-fill: white");
        
        if(tb_senha.getText().isEmpty())
        {
            tb_senha.setStyle("-fx-text-fill: red");
            error += "Senha[Empety]\n";
            result = false;
        }
        else
            tb_senha.setStyle("-fx-text-fill: white");
        
        if(cb_cidade.getValue() == null)
        {
            error += "Cidade[Empety]\n";
            result = false;
        }
        
        if(tb_local.getText().isEmpty())
        {
            tb_local.setStyle("-fx-text-fill: red");
            error += "Logradouro[Empety]\n";
            result = false;
        }
        else
            tb_local.setStyle("-fx-text-fill: white");
        
        return result;
    }
            
    private void unlockNovo()
    {
        disableField(false);
        check_ativo.setDisable(true);
        check_ativo.setSelected(true);
        cb_nivel.setDisable(false);
        cb_nivel.setValue(cb_nivel.getItems().get(0));
        cb_estado.setValue(cb_estado.getItems().get(0));
        loadCidade();
        
        btn_novo.setDisable(false);
        btn_editar.setDisable(true);
        btn_apagar.setDisable(true);
        disableSearch(true);
    }
    
    private void unlockEditar()
    {
        disableField(false);
        check_ativo.setDisable(false);
        
        btn_novo.setDisable(true);
        btn_editar.setDisable(false);
        btn_apagar.setDisable(true);
        disableSearch(true);
    }
    
    private void disableField(boolean flag)
    {
        tb_nome.setDisable(flag);
        tb_cpf.setDisable(flag);
        tb_fone.setDisable(flag);
        tb_email.setDisable(flag);
        tb_login.setDisable(flag);
        tb_senha.setDisable(flag);
        cb_estado.setDisable(flag);
        cb_cidade.setDisable(flag);
        tb_local.setDisable(flag);
    }
    
    private void disableSearch(boolean flag)
    {
        cb_filtro.setDisable(flag);
        tb_buscar.setDisable(flag);
        btn_buscar.setDisable(flag);
        table_func.setDisable(flag);
    }
    
    private void loadCidade()
    {
        cb_cidade.setItems(DALCidade.searchByEstado(cb_estado.getValue().getCodigo()));
        cb_cidade.setValue(cb_cidade.getItems().get(0));
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        ObservableList<String> list = FXCollections.observableArrayList();
        
        list.add("Basico");
        list.add("Gerente");
        list.add("ADM");
                
        cb_nivel.setItems(list);
        
        list = FXCollections.observableArrayList();
        list.add("Nome");
        list.add("CPF");
        list.add("Ativo");
        list.add("Inativo");
        
        cb_filtro.setItems(list);
        
        cb_estado.setItems(DALEstado.searchAll());
        cb_estado.setValue(cb_estado.getItems().get(0));
        loadCidade();
        cb_cidade.setValue(cb_cidade.getItems().get(0));
        
        //colunas
        c_cod.setCellValueFactory(new PropertyValueFactory<Funcionario, Integer>("codigo"));
        c_nome.setCellValueFactory(new PropertyValueFactory<Funcionario, String>("nome"));
        c_cpf.setCellValueFactory(new PropertyValueFactory<Funcionario, String>("cpf"));
        c_fone.setCellValueFactory(new PropertyValueFactory<Funcionario, String>("fone"));
        c_email.setCellValueFactory(new PropertyValueFactory<Funcionario, String>("email"));
        c_login.setCellValueFactory(new PropertyValueFactory<Funcionario, String>("login"));
        c_nivel.setCellValueFactory(new PropertyValueFactory<Funcionario, Integer>("senha"));
        c_ativo.setCellValueFactory(new PropertyValueFactory<Funcionario, Boolean>("ativo"));
        
        //campos
        MaskFieldUtil.onlyDigitsValue(tb_nome);
        MaskFieldUtil.cpfField(tb_cpf);
        MaskFieldUtil.foneField(tb_fone);
        
        refresh();
    }

    @FXML
    private void ClickNovo(ActionEvent event)
    {
        if(btn_novo.getText().compareTo("Novo") == 0)//liberar
        {
            select = null;
            btn_novo.setText("Salvar");
            btn_sair.setText("Cancelar");
            unlockNovo();
        }
        else//salvar
        {
            String nome = tb_nome.getText();
            String cpf = tb_cpf.getText();
            String fone = tb_fone.getText();
            String email = tb_email.getText();
            String login = tb_login.getText();
            String senha = tb_senha.getText();
            Estado estado = cb_estado.getValue();
            Cidade cidade = cb_cidade.getValue();
            String logradouro = tb_local.getText();
            int nivel = cb_nivel.getSelectionModel().getSelectedIndex();
            
            try
            {                
                if(checkFields())
                {
                    DALFuncionario.insert(nome, cpf, fone, email, login, senha, nivel, true, cidade, logradouro);
                    btn_novo.setText("Novo");
                    refresh();
                }
                else
                    new Alert(Alert.AlertType.ERROR, error, ButtonType.OK).showAndWait();
            }
            catch(Exception ex)
            {
                new Alert(Alert.AlertType.ERROR, ex.getMessage(), ButtonType.OK).showAndWait();
            }
        }
    }

    @FXML
    private void ClickEditar(ActionEvent event)
    {
        if(btn_editar.getText().compareTo("Editar") == 0)//liberar
        {
            btn_editar.setText("Salvar");
            btn_sair.setText("Cancelar");
            unlockEditar();
        }
        else//salvar
        {
            String error = "Campos Invalidos:\n\n";
            String nome = tb_nome.getText();
            String cpf = tb_cpf.getText();
            String fone = tb_fone.getText();
            String email = tb_email.getText();
            String login = tb_login.getText();
            String senha = tb_senha.getText();
            int nivel = cb_nivel.getSelectionModel().getSelectedIndex();
            boolean ativo = check_ativo.isSelected();
            Estado estado = cb_estado.getValue();
            Cidade cidade = cb_cidade.getValue();
            String logradouro = tb_local.getText();
            
            try
            {
                if(checkFields())
                {
                    DAL.DALFuncionario.update(select.getCodigo(), nome, cpf, fone, email, login, senha, nivel, ativo, cidade, login);
                    btn_editar.setText("Editar");
                    refresh();
                }
                else
                    new Alert(Alert.AlertType.ERROR, error, ButtonType.OK).showAndWait();
            }
            catch(Exception ex)
            {
                new Alert(Alert.AlertType.ERROR, ex.getMessage(), ButtonType.OK).showAndWait();
            }
        }
    }

    @FXML
    private void ClickApagar(ActionEvent event)
    {
        JOptionPane op = new JOptionPane("Confirmar exclusão?", JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_OPTION);
        
        DAL.DALFuncionario.delete(select.getCodigo());
        refresh();
    }

    @FXML
    private void ClickSair(ActionEvent event)
    {
        if(btn_sair.getText().compareTo("Cancelar") == 0)
            refresh();
        
    }

    @FXML
    private void ChangeFiltro(ActionEvent event)
    {
        
    }

    @FXML
    private void ClickBuscar(ActionEvent event)
    {
        if(tb_buscar.getText().isEmpty())
        {
            table_func.setItems(DAL.DALFuncionario.searchAll());
            
        }
        else
        {
            switch(cb_filtro.getSelectionModel().getSelectedIndex())
            {
                case 0: table_func.setItems(DAL.DALFuncionario.searchByName(tb_buscar.getText())); break; //nome                   
                case 1: table_func.setItems(DAL.DALFuncionario.searchByCPF(tb_buscar.getText())); break; //CPF
                case 2: table_func.setItems(DAL.DALFuncionario.searchByActive(true)); break; //Ativo
                case 3: table_func.setItems(DAL.DALFuncionario.searchByActive(false)); break; //Inativo
            }
        }
        
        table_func.setDisable(false);
    }

    @FXML
    private void ClickTable(MouseEvent event)
    {
        if(table_func.getSelectionModel().getSelectedIndex() >= 0)
        {
            select = table_func.getItems().get(table_func.getSelectionModel().getSelectedIndex());
            btn_novo.setDisable(true);
            btn_editar.setDisable(false);
            btn_apagar.setDisable(false);
            btn_sair.setText("Cancelar");
            loadFields();
        }
    }

    @FXML
    private void ChangeEstado(ActionEvent event)
    {
        loadCidade();
    }

}
