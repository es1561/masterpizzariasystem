package Scene;

import Class.Cidade;
import Class.Estado;
import Class.Funcionario;
import Class.MaskFieldUtil;
import Class.Produto;
import Class.Sistema;
import DAL.DALEstado;
import DAL.DALFuncionario;
import DAL.DALProduto;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javax.swing.JOptionPane;

public class FXMLProdutoController implements Initializable 
{
    private Produto select;
    private String error;
    
    @FXML
    private HBox hb_front_1;
    @FXML
    private JFXButton btn_novo;
    @FXML
    private JFXButton btn_editar;
    @FXML
    private JFXButton btn_apagar;
    @FXML
    private JFXButton btn_sair;
    @FXML
    private VBox vb_back;
    @FXML
    private JFXTextField tb_nome;
    @FXML
    private HBox hn_front_2;
    @FXML
    private ComboBox<String> cb_filtro;
    @FXML
    private TextField tb_buscar;
    @FXML
    private JFXButton btn_buscar;
    @FXML
    private JFXTextField tb_desc;
    @FXML
    private JFXTextField tb_valor;
    @FXML
    private ComboBox<String> cb_tipo;
    @FXML
    private TableView<Produto> table_func;
    @FXML
    private TableColumn<Produto, Integer> c_cod;
    @FXML
    private TableColumn<Produto, String> c_nome;
    @FXML
    private TableColumn<Produto, Double> c_valor;
    @FXML
    private TableColumn<Produto, Integer> c_estoque;
    @FXML
    private TableColumn<Produto, String> c_desc;
    @FXML
    private JFXButton btn_lotes;

    private void load()
    {
        //scene
        vb_back.setStyle("-fx-background-color:" + Sistema.getCor_fundo());
        hb_front_1.setStyle("-fx-background-color:" + Sistema.getCor_frente());
        hb_front_1.setStyle("-fx-background-color:" + Sistema.getCor_frente());
        
        //botoes
        btn_apagar.setStyle("-fx-background-color:" + Sistema.getCor_botao());
        btn_buscar.setStyle("-fx-background-color:" + Sistema.getCor_botao());
        btn_editar.setStyle("-fx-background-color:" + Sistema.getCor_botao());
        btn_novo.setStyle("-fx-background-color:" + Sistema.getCor_botao());
        btn_sair.setStyle("-fx-background-color:" + Sistema.getCor_botao());
        btn_lotes.setStyle("-fx-background-color:" + Sistema.getCor_botao());
    }
    
    private void refresh()
    {
        btn_novo.setDisable(false);
        btn_editar.setDisable(true);
        btn_apagar.setDisable(true);
        
        btn_novo.setText("Novo");
        btn_editar.setText("Editar");
        btn_sair.setText("Sair");
        
        disableField(true);
        clearFields();
        
        disableSearch(false);
        cb_filtro.setValue(cb_filtro.getItems().get(0));
        table_func.setItems(null);
    }
    
    private void clearFields()
    {
        tb_nome.clear();
        tb_desc.clear();
        tb_valor.clear();
        cb_tipo.setValue(cb_tipo.getItems().get(0));
    }
    
    private void loadFields()
    {
        tb_nome.setText(select.getNome());
        tb_desc.setText(select.getDescricao());
        tb_valor.setText(String.valueOf(select.getValor()));
        cb_tipo.setValue(cb_tipo.getItems().get(select.getTipo() == 'P' ? 0 : 1));
    }
    
    private void disableField(boolean flag)
    {
        tb_nome.setDisable(flag);
        tb_desc.setDisable(flag);
        btn_lotes.setDisable(flag);
        tb_valor.setDisable(flag);
        cb_tipo.setDisable(flag);
    }
    
    private void disableSearch(boolean flag)
    {
        cb_filtro.setDisable(flag);
        tb_buscar.setDisable(flag);
        btn_buscar.setDisable(flag);
        table_func.setDisable(flag);
    }
    
    private void unlockNovo()
    {
        disableField(false);
        cb_filtro.setValue(cb_filtro.getItems().get(0));
        
        btn_novo.setDisable(false);
        btn_editar.setDisable(true);
        btn_apagar.setDisable(true);
        disableSearch(true);
    }
    
    private void unlockEditar()
    {
        disableField(false);
        
        btn_novo.setDisable(true);
        btn_editar.setDisable(false);
        btn_apagar.setDisable(true);
        disableSearch(true);
    }
    
    private boolean checkFields()
    {
        boolean result = true;
        error = "Campos Invalidos:\n\n";
        
        if(tb_nome.getText().isEmpty())
        {
            tb_nome.setStyle("-fx-text-fill: red");
            error += "Nome[Empety]\n";
            result = false;
        }
        else
            tb_nome.setStyle("-fx-text-fill: white");
        
        if(tb_valor.getText().isEmpty() || (!tb_valor.getText().isEmpty() && Double.valueOf(tb_valor.getText().replace(".", "").replace(",", ".")) < 0))
        {
            tb_valor.setStyle("-fx-text-fill: red");
            error += tb_valor.getText().isEmpty() ? "Valor[Empety]\n" : "Valor[Invalid]\n";
            result = false;
        }
        else
            tb_valor.setStyle("-fx-text-fill: white");

        return result;
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) 
    {
        ObservableList<String> list = FXCollections.observableArrayList();
        
        list.add("Produto");
        list.add("Insumo");
                
        cb_tipo.setItems(list);
        
        list = FXCollections.observableArrayList();
        list.add("Nome");
        list.add("Produto");
        list.add("Insumo");
        
        cb_filtro.setItems(list);
        
        //colunas
        c_cod.setCellValueFactory(new PropertyValueFactory<Produto, Integer>("codigo"));
        c_nome.setCellValueFactory(new PropertyValueFactory<Produto, String>("nome"));
        c_valor.setCellValueFactory(new PropertyValueFactory<Produto, Double>("valor"));
        c_estoque.setCellValueFactory(new PropertyValueFactory<Produto, Integer>("estoque"));
        c_desc.setCellValueFactory(new PropertyValueFactory<Produto, String>("desc"));
        
        //campos
        MaskFieldUtil.monetaryField(tb_valor);

        refresh();
    }    

    @FXML
    private void ClickNovo(ActionEvent event)
    {
        if(btn_novo.getText().compareTo("Novo") == 0)//liberar
        {
            select = null;
            btn_novo.setText("Salvar");
            btn_sair.setText("Cancelar");
            unlockNovo();
        }
        else//salvar
        {
            String nome = tb_nome.getText();
            Double valor = Double.valueOf(tb_valor.getText().replace(".", "").replace(",", "."));
            String desc = tb_desc.getText();
            char tipo = cb_tipo.getSelectionModel().getSelectedIndex() == 0 ? 'P' : 'I';

            
            try
            {
                if(checkFields())
                {
                    DALProduto.insert(nome, valor, tipo, desc);
                    btn_novo.setText("Novo");
                    refresh();
                }
                else
                    new Alert(Alert.AlertType.ERROR, error, ButtonType.OK).showAndWait();
            }
            catch(Exception ex)
            {
                new Alert(Alert.AlertType.ERROR, ex.getMessage(), ButtonType.OK).showAndWait();
            }
        }
    }

    @FXML
    private void ClickEditar(ActionEvent event)
    {
        if(btn_editar.getText().compareTo("Editar") == 0)//liberar
        {
            btn_editar.setText("Salvar");
            btn_sair.setText("Cancelar");
            unlockEditar();
        }
        else//salvar
        {
            String nome = tb_nome.getText();
            Double valor = Double.valueOf(tb_valor.getText().replace(".", "").replace(",", "."));
            String desc = tb_desc.getText();
            char tipo = cb_tipo.getSelectionModel().getSelectedIndex() == 0 ? 'P' : 'I';

            
            try
            {
                if(checkFields())
                {
                    DALProduto.update(select.getCodigo(), nome, valor,tipo, desc);
                    btn_editar.setText("Editar");
                    refresh();
                }
                else
                    new Alert(Alert.AlertType.ERROR, error, ButtonType.OK).showAndWait();
            }
            catch(Exception ex)
            {
                new Alert(Alert.AlertType.ERROR, ex.getMessage(), ButtonType.OK).showAndWait();
            }
        }
    }

    @FXML
    private void ClickApagar(ActionEvent event) throws SQLException
    {
        JOptionPane op = new JOptionPane("Confirmar exclusão?", JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_OPTION);
        
        DALProduto.delete(select.getCodigo());
        refresh();
    }

    @FXML
    private void ClickSair(ActionEvent event)
    {
        if(btn_sair.getText().compareTo("Cancelar") == 0)
            refresh();
    }

    @FXML
    private void ChangeFiltro(ActionEvent event)
    {
        
    }

    @FXML
    private void ClickBuscar(ActionEvent event)
    {
        if(tb_buscar.getText().isEmpty())
        {
            table_func.setItems(DALProduto.searchAll());
            
        }
        else
        {
            switch(cb_filtro.getSelectionModel().getSelectedIndex())
            {
                case 0: table_func.setItems(DALProduto.searchByNome(tb_buscar.getText())); break; //nome                   
                case 1: table_func.setItems(DALProduto.searchByTipo('P')); break; //produto
                case 2: table_func.setItems(DALProduto.searchByTipo('I')); break; //insumo
            }
        }
    }

    @FXML
    private void ClickTable(MouseEvent event)
    {
        if(table_func.getSelectionModel().getSelectedIndex() >= 0)
        {
            select = table_func.getItems().get(table_func.getSelectionModel().getSelectedIndex());
            btn_novo.setDisable(true);
            btn_editar.setDisable(false);
            btn_apagar.setDisable(false);
            btn_sair.setText("Cancelar");
            loadFields();
        }
    }

    @FXML
    private void ClickLote(ActionEvent event)
    {
        
    }
    
}
