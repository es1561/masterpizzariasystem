package DataBase;

public class Banco
{

    static private Conexao con = null;

    private Banco()
    {
    }

    public static Conexao getCon()
    {
        return con;
    }

    public static boolean conectar()
    {
        if (con == null)
        {
            con = new Conexao();
            
            if(con.conectar("jdbc:postgresql://localhost:5432/", "Master", "postgres", "postgres123"))
                return true;

            /*
            
            else
            {
                criarBD();
                return con.conectar("jdbc:postgresql://localhost:5432/", "Master", "postgres", "postgres123");
            }
            
            */
        }
        
        return false;
    }

    /*
    
    private static boolean criarBD() 
    {
        try 
        {
            //Connection conn = Persistence.createEntityManagerFactory("PostgresStart").createEntityManager().unwrap(java.sql.Connection.class);
            Connection conn
                    = DriverManager.getConnection("jdbc:postgresql://localhost:5432/", "postgres", "postgres123");
            Statement stmt = conn.createStatement();
            int Linhas = stmt.executeUpdate("CREATE DATABASE " + "Boteco"
                    + " WITH OWNER = postgres ENCODING = 'UTF8' CONNECTION LIMIT = -1;");
            System.out.println(Linhas);
        } 
        catch (Exception e) 
        {
            return false;
        }
        return true;
    }
    
    public void backup()
    {
        executarCMD("backup.bat");
    }
    
    public void restore()
    {
        executarCMD("restore.bat");
    }
    
    private boolean executarCMD(String cmd) 
    {
        Runtime r = Runtime.getRuntime();
        
        try 
        {
            Process p = r.exec("util\\" + cmd);
            if (p != null) 
            {
                InputStreamReader str = new InputStreamReader(p.getErrorStream());
                BufferedReader reader = new BufferedReader(str);
                String linha;
                while ((linha = reader.readLine()) != null) 
                {
                    System.out.println(linha);
                }
                System.out.println("Completo....." + "\n");
            }
        } 
        catch (IOException ex) 
        {
            
            return false;
        }
        return true;
    }
    
    */
    
}
